import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilmsComponent } from './pages/films/films.component';
import { GestionComponent } from './pages/gestion/gestion.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  {
    path: "", pathMatch: "full", component: HomeComponent
  },
  {
    path: "home", pathMatch: "full", component: HomeComponent
  },
  {
    path: "films", pathMatch: "full", component: FilmsComponent
  },
  {
    path: "gestion", pathMatch: "full", component: GestionComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
