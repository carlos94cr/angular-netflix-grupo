import { Component, OnInit } from '@angular/core';
import { HomeInterface } from 'src/app/service/netflix.interfaces';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
public homeInfo: HomeInterface;

  constructor() {
    this.homeInfo = {
      title: "Top 10",
      gallery: [{
        src: "../../assets/Top10/1-papel.webp",
        alt: "La casa de papel"
      },
      {
        src: "../../assets/Top10/2-reina.webp",
        alt: "La reina del flow"
      },
      {
        src: "../../assets/Top10/3-titanes.webp",
        alt: "Titanes"
      },
      {
        src: "../../assets/Top10/4-lostinspace.webp",
        alt: "Lost in space"
      },
      {
        src: "../../assets/Top10/5-dondecaben.webp",
        alt: "Donde caben dos"
      },
      {
        src: "../../assets/Top10/6-aquinohay.webp",
        alt: "Aquí no hay quién viva"
      },
      {
        src: "../../assets/Top10/7-blacklist.webp",
        alt: "Blacklist"
      },
      {
        src: "../../assets/Top10/8-ricos.webp",
        alt: "Ricos y mimados"
      },
      {
        src: "../../assets/Top10/9-poder.webp",
        alt: "El poder del perro"
      },
      {
        src: "../../assets/Top10/10-gooddoctor.webp",
        alt: "The Good Doctor"
      }]
    }
  }

  ngOnInit(): void {
  }

}
