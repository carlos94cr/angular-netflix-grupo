import { FilmsInterface } from './../../../model/films.interface';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.scss']
})
export class GenreComponent implements OnInit {
  @Input() public films!: FilmsInterface[]
  @Input() public titels!: FilmsInterface[]

  constructor() { }

  ngOnInit(): void {
  }

}
