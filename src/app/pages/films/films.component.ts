import { Component, OnInit } from '@angular/core';
import { FilmsInterface } from 'src/app/model/films.interface';
import { ComedyService } from 'src/app/service/comedy/comedy.service';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss']
})
export class FilmsComponent implements OnInit {
  public terrorList: FilmsInterface[] = []
  public comedyList: FilmsInterface[] = []
  public dramaList: FilmsInterface[] = []
  public actionList: FilmsInterface[] = []
  public titleList: FilmsInterface[] = []

  constructor(private comedyService: ComedyService) { }

  ngOnInit(): void {
    this.comedyService.getComedy().subscribe((data: any) => {
      this.terrorList = data.terror
      this.comedyList = data.comedy
      this.dramaList = data.drama
      this.actionList = data.action
    })
  }

}
