export interface FilmsInterface {
    id: number,
    title: string,
    director: string,
    cover: string
}