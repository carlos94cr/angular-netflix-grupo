import { Component, OnInit } from '@angular/core';
import { NavigatorInterface } from 'src/app/service/netflix.interfaces';

@Component({
  selector: 'app-navigator',
  templateUrl: './navigator.component.html',
  styleUrls: ['./navigator.component.scss'],
})
export class NavigatorComponent implements OnInit {
  public navigatorInfo: NavigatorInterface;

  constructor() {
    this.navigatorInfo = {
      logo: {
        src: '../../../assets/Logo/logo.png',
        alt: 'Netflix',
      },
      search: {
        src: '../../../assets/Icons/search.png',
        alt: 'Buscar',
      },
      notifications: {
        src: '../../../assets/Icons/notifications.png',
        alt: 'Notificaciones',
      },
    };
  }

  ngOnInit(): void {}

  public changeTheme = () => {
    document.body.classList.toggle('light');
  };
}