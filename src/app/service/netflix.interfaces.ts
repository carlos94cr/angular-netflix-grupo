export interface NavigatorInterface {
    logo: ImageInterface;
    search: ImageInterface;
    notifications: ImageInterface;
}

export interface HomeInterface {
    title: string;
    gallery: ImageInterface[]
}

export interface ImageInterface {
    src: string;
    alt: string;
}

// export interface FilmsInterface{
//     title: string;
//     gallery: ImageInterface[]
// }
