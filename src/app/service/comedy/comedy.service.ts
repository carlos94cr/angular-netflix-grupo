import { FilmsInterface } from './../../model/films.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ComedyService {
  public BASEURL: string = 'http://localhost:3000/movies'

  constructor(private httpClient: HttpClient) { }

  public comedyData: FilmsInterface = {
    id: 0,
    title: "",
    director: "",
    cover: ""
  }

  public getComedy() {
    return this.httpClient.get(this.BASEURL)
  }
}
